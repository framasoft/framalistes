/* eslint-disable import/no-commonjs */
/* eslint-disable import/no-extraneous-dependencies */
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const PrerenderSPAPlugin = require('prerender-spa-plugin');
const Renderer = require('@prerenderer/renderer-jsdom');
const TerserPlugin = require('terser-webpack-plugin');

function buildConfig(env) {
  const config = Object.assign({}, require('./common')(env)); // eslint-disable-line

  config.output.filename = '[name].[hash].bundle.js';

  Object.assign(config, {
    devtool: '#source-map',
    mode: 'production',
    optimization: {
      minimizer: [
        new TerserPlugin({
          cache: true,
          parallel: true,
          sourceMap: true, // set to true if you want JS source maps
        }),
        new OptimizeCSSAssetsPlugin({}),
      ],
      splitChunks: {
        chunks: 'all',
      },
    },
  });

  config.plugins.push(
    new HtmlWebpackPlugin({
      title: 'PRODUCTION prerender-spa-plugin',
      template: 'public/index.html',
      filename: `${env.dist}/index.html`,
    }),
    new MiniCssExtractPlugin({
      filename: '[name].[hash].css',
      chunkFilename: '[id].[hash].css',
    }),
    new PrerenderSPAPlugin({
      staticDir: env.dist,
      routes: env.routes,
      renderer: new Renderer({
        maxConcurrentRoutes: 1,
        renderAfterElementExists: 'meta[name="description"]', // when vue-meta is ok
      }),
      postProcess(renderedRoute) {
        // eslint-disable-next-line no-param-reassign
        renderedRoute.html = renderedRoute.html
          .replace(/<script (.*?)>/g, '<script $1 defer>')
          .replace('id="app"', 'id="app" data-server-rendered="true"');

        return renderedRoute;
      },
    }),
  );

  return config;
}

module.exports = buildConfig;
